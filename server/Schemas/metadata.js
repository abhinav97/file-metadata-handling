import mongoose from "mongoose";

const metadataSchema = mongoose.Schema({

    name : String,

    modifiedAt : String,

    createdAt :  String,

    type : String

});

const metadataModel = mongoose.model("metadataCollection", metadataSchema, "metadataCollection");

export default metadataModel;