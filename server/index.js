import Connection from './database/db.js';
import modelData from './Schemas/metadata.js';
import fs from 'fs';
import path from 'path';


const fileHandler = async() => {

    const dir = './Directory'

    const files = fs.readdirSync(dir);

    for(const file of files){

    const name =  path.basename(file, path.extname(file));

    let type='';

    if(path.extname(file)){
        type = path.extname(file).split(".")[1];
    }
    else {
         type = 'folder';
    }


    const stat = fs.lstatSync(path.join(dir, file));

    const modifiedAt = stat.ctime.toString();

     const createdAt = stat.birthtime.toString();

    const existingFile= await modelData.findOne({createdAt : createdAt});

    if(existingFile){

        await modelData.updateOne({createdAt: createdAt},{type : type, modifiedAt: modifiedAt, name: name})
        // console.log("Data updated")

    }
    else
    {

    const newData = new modelData({name : name, modifiedAt : modifiedAt, type : type, createdAt : createdAt});


    await newData.save();

    // console.log("Data is saved");

    }


}

const allData = await modelData.find({});

for(const x of allData){

    let fileName = x.name;

    let type = x.type;

    if(files.includes(`${fileName}`+`.`+`${type}`)){

        continue;
    }

    else if(files.includes(`${fileName}`)){
        
        continue;
    }

    else{
        await modelData.deleteOne({name : fileName});
    }
    
}

}



Connection('user', 'mycrudapplication');

setTimeout(fileHandler, 300000);
